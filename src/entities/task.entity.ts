import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class TaskEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 30 })
  email: string;

  @Column({ length: 300 })
  text: string;

  @Column({ type: 'boolean', default: false })
  status;
}
