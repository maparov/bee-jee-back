import { Injectable } from '@nestjs/common';
import { TaskDto } from 'src/dto/task.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskEntity } from '../entities/task.entity';

const PAGE_LIMIT = 3;

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(TaskEntity)
    private readonly taskRepository: Repository<TaskEntity>,
  ) {}

  async findTasks(page: number) {
    const result = await this.taskRepository.find({
      take: PAGE_LIMIT,
      skip: page,
      order: {
        name: 'ASC',
      },
    });
    const tasks = result.map((item) => ({
      name: item.name,
      email: item.email,
      text: item.text,
      status: item.status,
    }));
    return tasks;
  }

  async createTask(task: TaskDto) {
    const { name, email, text, status } = task;
    const newTask = new TaskEntity();
    newTask.name = name;
    newTask.email = email;
    newTask.text = text;
    newTask.status = status;
    await this.taskRepository.save(newTask);

    return 'ok';
  }
}
