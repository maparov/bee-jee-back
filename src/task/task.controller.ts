import {
  Controller,
  Post,
  Get,
  Put,
  Body,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskDto } from 'src/dto/task.dto';

@Controller('api')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post('task')
  createTask(@Body() task: TaskDto) {
    return this.taskService.createTask(task);
  }

  @Put('task')
  updateTask(@Body() task: TaskDto) {
    return 'to be done ';
  }

  @Get('tasks')
  findTasks(@Query('page', ParseIntPipe) page: number) {
    return this.taskService.findTasks(page);
  }
}
