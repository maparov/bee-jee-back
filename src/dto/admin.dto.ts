import { IsEmail, IsString, Matches } from 'class-validator';

export class AdminDto {
  @IsEmail()
  email: string;

  @IsString()
  @Matches(/^123$/, {
    message: 'Invalid credentials',
  })
  password: string;
}
