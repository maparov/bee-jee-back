import { IsString, IsEmail, IsBoolean } from 'class-validator';

export class TaskDto {
  @IsString()
  name: string;
  @IsEmail()
  email: string;
  @IsString()
  text: string;
  @IsBoolean()
  status: boolean;
}
