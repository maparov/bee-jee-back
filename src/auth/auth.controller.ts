import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { Response } from 'express';
import { AdminDto } from 'src/dto/admin.dto';
import * as jwt from 'jsonwebtoken';

const PRIVATE_KEY = 'ABCD';

@Controller('auth')
export class AuthController {
  @Post('login')
  login(@Body() admin: AdminDto, @Res() res: Response) {
    // for test purpose only
    const payload = { admin: true };
    const expiresIn = '7d';
    const jwtToken = jwt.sign(payload, PRIVATE_KEY, {
      expiresIn: expiresIn,
    });

    res
      .status(HttpStatus.CREATED)
      .cookie('token', jwtToken, {
        httpOnly: true,
        sameSite: true,
      })
      .json({ done: true });
  }
}
