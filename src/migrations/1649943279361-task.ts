import {MigrationInterface, QueryRunner} from "typeorm";

export class task1649943279361 implements MigrationInterface {
    name = 'task1649943279361'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "task_entity" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying(50) NOT NULL, "email" character varying(30) NOT NULL, "text" character varying(300) NOT NULL, "status" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_0385ca690d1697cdf7ff1ed3c2f" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "task_entity"`);
    }

}
