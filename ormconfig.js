require('dotenv').config();

const folder =
  process.env.npm_lifecycle_event === 'typeorm:cli-ts' ? 'src' : 'dist';

module.exports = {
  type: 'postgres',
  host: 'localhost',
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: false,
  entities: [`./${folder}/**/*.entity.{js,ts}`],
  migrations: [`./${folder}/migrations/*.{js,ts}`],
  cli: {
    entitiesDir: 'src/entities',
    migrationsDir: 'src/migrations',
  },
};
